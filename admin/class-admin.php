<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://dev-o-matic.com/
 * @since      0.1.0
 *
 * @package    Dom/Woo_Customize_Login
 * @subpackage Dom/Woo_Customize_Login/Admin
 */

namespace DOM\Woo_Customize_Login\Admin;

use DOM\Woo_Customize_Login\Includes\Loader;

class Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Loader instance.
	 *
	 * @since 0.1.0
	 * @access protected
	 * @var Loader
	 */
	protected $loader;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @param string $plugin_name The name of this plugin.
	 * @param string $version The version of this plugin.
	 * @param Loader $loader Loader instance.
	 *
	 * @since    0.1.0
	 */
	public function __construct( $plugin_name, $version, Loader $loader ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;
		$this->loader      = $loader;

		$this->maybe_show_notices();
	}

	protected function maybe_show_notices() {
		if ( defined( 'DOING_AJAX' ) === false || DOING_AJAX === false ) {
			$admin_notices = new Admin_Notices();
			if ( method_exists( $admin_notices, $admin_notices->get_notice_name() ) === true ) {
				$this->loader->add_action( 'admin_notices', $admin_notices, $admin_notices->get_notice_name() );
			}
		}
	}

}
