<?php

/**
 * Parse, define, prepare and hook admin notices.
 *
 * @link       https://dev-o-matic.com/
 * @since      0.1.0
 *
 * @package    Dom/Woo_Customize_Login
 * @subpackage Dom/Woo_Customize_Login/Admin
 */

namespace DOM\Woo_Customize_Login\Admin;

class Admin_Notices {

	protected $notice_type;
	protected $notice_name;

	public function __construct() {
		$this->parse_request();
	}

	public function parse_request() {
		if ( isset( $_REQUEST['dom_notice'] ) ) {
			$this->notice_type = sanitize_text_field( $_REQUEST['dom_notice'] );
			$this->notice_name = sanitize_text_field( $_REQUEST[ 'dom_' . $this->notice_type ] );
		}
	}

	public function get_notice_name() {
		return $this->notice_name;
	}

	public function plugin_dependency_failed() {
		$plugin_name       = __( 'Woo Customize Login', DOM_WOO_CUSTOMIZE_LOGIN_TEXT_DOMAIN );
		$dependency_plugin = __( 'WooCommerce', DOM_WOO_CUSTOMIZE_LOGIN_TEXT_DOMAIN );

		echo '<div class="error"><p>';
		echo sprintf( __( '%1$s requires %2$s to function correctly. Please activate %2$s before activating %1$s. For now, the plugin has been deactivated.', DOM_WOO_CUSTOMIZE_LOGIN_TEXT_DOMAIN ),
			'<strong>' . esc_html( $plugin_name ) . '</strong>',
			'<strong>' . esc_html( $dependency_plugin ) . '</strong>' );
		echo '</p></div>';
	}
}