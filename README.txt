=== Plugin Name ===
Contributors: nemanjac
Donate link: https://dev-o-matic.com/
Tags: woocommerce, custom, auth, login, register, page
Requires at least: 5.2
Tested up to: 5.2
Stable tag: 5.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Add custom login and register pages on custom urls. Easier to customize, and adds separate page for registration
compared to WooCommerce

== Description ==

Adds missing feature to WooCommerce where you can style login and registration pages by copying files from plugin
to woocommerce directory in theme.

Plugin takes over myaccount login screen, and adds missing registration screen.
URIs on which this pages can be found are static for now, and they are as follows:

- auth/login
- auth/register

Non-logged-in visitors who open myaccount page will be redirected to /auth/login
After login, in case cart is not empty, customer will be redirected to cart page, or to myaccount page.

Available hooks:
[list them here]

== Installation ==

1. Upload `dom-woo-customize-login` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==

= 0.1 =
* Initial release.
* Defines login and registration pages on /auth/login and /auth/register uris.
* Defines road-map for new releases
