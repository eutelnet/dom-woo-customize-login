<?php

/**
 * Set custom rewrite rules
 *
 * @link       https://dev-o-matic.com/
 * @since      0.1.0
 *
 * @package    Dom/Woo_Customize_Login
 * @subpackage Dom/Woo_Customize_Login/Includes
 */

namespace DOM\Woo_Customize_Login\Includes;

class Rewrite_Rules {

	/**
	 * Add custom rewrite rules
	 *
	 * @param $wp_rewrite
	 *
	 * @todo Add support for reset password, generate new password, and password successfully changed pages
	 */
	public function add_auth_rewrite_rules( $wp_rewrite ) {
		$wp_rewrite->rules = array_merge( [
			'auth/login'    => 'index.php?dom_auth_page=login',
			'auth/register' => 'index.php?dom_auth_page=register',
		], $wp_rewrite->rules );
	}
}