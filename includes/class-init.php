<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://dev-o-matic.com/
 * @since      0.1.0
 *
 * @package    Dom/Woo_Customize_Login
 * @subpackage Dom/Woo_Customize_Login/Includes
 */

namespace DOM\Woo_Customize_Login\Includes;

use DOM\Woo_Customize_Login\Admin\Admin;
use DOM\Woo_Customize_Login\Front\Front;

class Init {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    0.1.0
	 * @access   protected
	 * @var      Loader $loader Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    0.1.0
	 * @access   protected
	 * @var      string $plugin_name The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    0.1.0
	 * @access   protected
	 * @var      string $version The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    0.1.0
	 */
	public function __construct() {
		if ( defined( 'DOM_WOO_CUSTOMIZE_LOGIN_VERSION' ) ) {
			$this->version = DOM_WOO_CUSTOMIZE_LOGIN_VERSION;
		} else {
			$this->version = '0.1.0';
		}
		$this->plugin_name = 'dom-woo-customize-login';

		$this->loader = new Loader();

		$this->load_rewrite_rules();
		$this->prepare_query_vars();

		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_front_hooks();
	}

	private function load_rewrite_rules() {
		$rewrite = new Rewrite_Rules();
		$this->loader->add_filter( 'generate_rewrite_rules', $rewrite, 'add_auth_rewrite_rules' );
	}

	private function prepare_query_vars() {
		$query_vars = new Query_Vars();
		$this->loader->add_filter( 'query_vars', $query_vars, 'add_dom_auth_page_query_var' );
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Dom_Woo_Customize_Login_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function set_locale() {
		$plugin_i18n = new I18n();
		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function define_admin_hooks() {
		if ( is_admin() === true ) {
			$plugin_admin = new Admin( $this->get_plugin_name(), $this->get_version(), $this->loader );
		}

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function define_front_hooks() {
		if ( is_admin() === false ) {
			$front = new Front( $this->get_plugin_name(), $this->get_version(), $this->loader );
		}

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    0.1.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @return    string    The name of the plugin.
	 * @since     0.1.0
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @return    Loader    Orchestrates the hooks of the plugin.
	 * @since     0.1.0
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @return    string    The version number of the plugin.
	 * @since     0.1.0
	 */
	public function get_version() {
		return $this->version;
	}

}
