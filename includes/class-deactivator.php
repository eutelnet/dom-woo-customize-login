<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://dev-o-matic.com/
 * @since      0.1.0
 *
 * @package    Dom/Woo_Customize_Login
 * @subpackage Dom/Woo_Customize_Login/Includes
 */

namespace DOM\Woo_Customize_Login\Includes;

class Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.1.0
	 */
	public static function deactivate() {

	}

	public function force_deactivate() {
		deactivate_plugins( plugin_basename( __FILE__ ) );
		if ( isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}
	}
}
