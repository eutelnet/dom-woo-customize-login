<?php

/**
 * Check for plugin dependencies on other plugins
 *
 * @link       https://dev-o-matic.com/
 * @since      0.1.0
 *
 * @package    Dom/Woo_Customize_Login
 * @subpackage Dom/Woo_Customize_Login/Includes
 */

namespace DOM\Woo_Customize_Login\Includes;

class Plugin_Dependencies {

	public function check() {
		if ( class_exists( 'WooCommerce' ) === false ) {
			$deactivator = new Deactivator();
			if ( current_user_can( 'activate_plugins' ) ) {
				$deactivator->force_deactivate();
			}

			$url = 'plugin-install.php?s=woocommerce&tab=search&type=term&dom_notice=error&dom_error=plugin_dependency_failed';
			if ( is_network_admin() ) {
				$url = 'network/' . $url;
			}
			wp_redirect( admin_url( $url ) );
			exit;
		}
	}
}