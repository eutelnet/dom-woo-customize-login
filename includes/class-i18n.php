<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://dev-o-matic.com/
 * @since      0.1.0
 *
 * @package    Dom/Woo_Customize_Login
 * @subpackage Dom/Woo_Customize_Login/Includes
 */

namespace DOM\Woo_Customize_Login\Includes;

class I18n {

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    0.1.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			DOM_WOO_CUSTOMIZE_LOGIN_TEXT_DOMAIN,
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}


}
