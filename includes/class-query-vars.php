<?php

/**
 * Set custom query vars
 *
 * @link       https://dev-o-matic.com/
 * @since      0.1.0
 *
 * @package    Dom/Woo_Customize_Login
 * @subpackage Dom/Woo_Customize_Login/Includes
 */

namespace DOM\Woo_Customize_Login\Includes;

class Query_Vars {

	public function add_dom_auth_page_query_var($query_vars)
	{
		$query_vars[] = "dom_auth_page";
		return $query_vars;
	}
}