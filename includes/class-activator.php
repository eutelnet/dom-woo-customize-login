<?php

/**
 * Fired during plugin activation
 *
 * @link       https://dev-o-matic.com/
 * @since      0.1.0
 *
 * @package    Dom/Woo_Customize_Login
 * @subpackage Dom/Woo_Customize_Login/Includes
 */

namespace DOM\Woo_Customize_Login\Includes;

class Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.1.0
	 */
	public static function activate() {
		$plugin_dependencies = new Plugin_Dependencies();
		$plugin_dependencies->check();
	}

}
