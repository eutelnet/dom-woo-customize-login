<?php

/**
 * Virtual auth pages loader.
 *
 * @link       https://dev-o-matic.com/
 * @since      0.1.0
 *
 * @package    Dom/Woo_Customize_Login
 * @subpackage Dom/Woo_Customize_Login/Front
 */

namespace DOM\Woo_Customize_Login\Front;

class Auth_Templates {
	public function load_virtual_page_template( $template ) {
		global $wp_query;
		$auth_page_template = '';

		if ( array_key_exists( 'dom_auth_page', $wp_query->query_vars ) === true ) {
			$default_path = plugin_dir_path( dirname( __FILE__ ) ) . 'templates/';
			switch ( $wp_query->query_vars['dom_auth_page'] ) {
				case 'login':
					$auth_page_template = wc_locate_template( 'dom-auth-pages/dom-login.php', '', $default_path );
					break;
				case 'register':
					$auth_page_template = wc_locate_template( 'dom-auth-pages/dom-register.php', '', $default_path );
					break;
				default:
			}

			if ( empty( $auth_page_template ) === false ) {
				$template = $auth_page_template;
			}
		}

		return $template;
	}
}