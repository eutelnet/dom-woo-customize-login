<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://dev-o-matic.com/
 * @since      0.1.0
 *
 * @package    Dom/Woo_Customize_Login
 * @subpackage Dom/Woo_Customize_Login/Front
 */

namespace DOM\Woo_Customize_Login\Front;

use DOM\Woo_Customize_Login\Includes\Loader;

class Front {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Loader instance.
	 *
	 * @since 0.1.0
	 * @access protected
	 * @var Loader
	 */
	protected $loader;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @param string $plugin_name The name of the plugin.
	 * @param string $version The version of this plugin.
	 * @param Loader $loader Loader instance.
	 *
	 * @since    0.1.0
	 */
	public function __construct( $plugin_name, $version, Loader $loader ) {
		$this->plugin_name = $plugin_name;
		$this->version     = $version;
		$this->loader      = $loader;

		$this->load_auth_templates();
		$this->prepare_redirections();
	}

	private function load_auth_templates() {
		$auth_templates = new Auth_Templates();
		$this->loader->add_filter( 'template_include', $auth_templates, 'load_virtual_page_template' );
	}

	private function prepare_redirections() {
		$auth_redirections = new Auth_Redirections();
		$this->loader->add_action( 'parse_request', $auth_redirections, 'process_request' );
		$this->loader->add_filter( 'woocommerce_login_redirect', $auth_redirections, 'after_auth_redirect' );
		$this->loader->add_filter( 'woocommerce_registration_redirect', $auth_redirections, 'after_auth_redirect' );
	}
}
