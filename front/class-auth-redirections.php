<?php

/**
 * Various redirection rules before and after auth (login/register).
 *
 * @link       https://dev-o-matic.com/
 * @since      0.1.0
 *
 * @package    Dom/Woo_Customize_Login
 * @subpackage Dom/Woo_Customize_Login/Front
 */

namespace DOM\Woo_Customize_Login\Front;

class Auth_Redirections {
	public function process_request( $query ) {
		$my_account_page       = get_post( get_option( 'woocommerce_myaccount_page_id' ) );
		$checkout_page         = get_post( get_option( 'woocommerce_checkout_page_id' ) );
		$enable_guest_checkout = ( get_option( 'woocommerce_enable_guest_checkout', 'no' ) === 'yes' ) ? true : false;

		if ( is_user_logged_in() === true && isset( $query->query_vars['dom_auth_page'] ) === true ) {
			wp_redirect( get_permalink( $my_account_page ) );
			exit;
		}

		if ( is_user_logged_in() === false && $query->request === $my_account_page->post_name ) {
			$location = apply_filters( 'dom_auth_anonymous_user_myaccount_redirects_to', 'auth/login' );
			wp_redirect( get_site_url( null, $location ) );
			exit;
		}

		if ( is_user_logged_in() === false && $query->request === $checkout_page->post_name && $enable_guest_checkout === false ) {
			$location = apply_filters( 'dom_auth_anonymous_user_checkout_redirects_to', 'auth/register' );
			wp_redirect( get_site_url( null, $location ) );
			exit;
		}
	}

	public function after_auth_redirect( $redirect ) {
		if ( WC()->cart->get_cart_contents_count() > 0 ) {
			//redirect to checkout
			$redirect = get_permalink( get_option( "woocommerce_checkout_page_id" ) );
		} else {
			//redirect to my-account
			$redirect = get_permalink( get_option( "woocommerce_myaccount_page_id" ) );
		}

		return $redirect;
	}
}