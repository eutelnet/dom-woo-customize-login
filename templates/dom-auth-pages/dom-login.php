<?php

if ( ! defined( "ABSPATH" ) ) {
	exit; // Exit if accessed directly.
}
?>

<?php get_header( apply_filters( 'dom_login_header_name', '' ) ) ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<article id="page-login" class="<?php echo apply_filters( 'dom_login_article_class', 'dom-login' ) ?>">

				<?php do_action( "dom_before_customer_login_form" ); ?>
				<?php do_action( "woocommerce_before_customer_login_form" ); ?>
				<div class="login-wrapper">
					<form class="woocommerce-form woocommerce-form-login login" method="post">

						<?php do_action( "dom_login_form_start" ); ?>
						<?php do_action( "woocommerce_login_form_start" ); ?>

						<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
							<label for="username">
								<?php esc_html_e( "Username or email address", "woocommerce" ); ?>&nbsp;
								<span class="required">*</span></label>
							<input type="text" name="username" id="username" autocomplete="username"
								   class="woocommerce-Input woocommerce-Input--text input-text"
								   value="<?php echo ( ! empty( $_POST["username"] ) ) ? esc_attr( wp_unslash( $_POST["username"] ) ) : ""; ?>"/><?php // @codingStandardsIgnoreLine ?>
						</p>
						<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
							<label for="password">
								<?php esc_html_e( "Password", "woocommerce" ); ?>&nbsp;
								<span class="required">*</span></label>
							<input type="password" name="password" id="password" autocomplete="current-password"
								   class="woocommerce-Input woocommerce-Input--text input-text"/>
						</p>

						<?php do_action( "dom_login_form" ); ?>
						<?php do_action( "woocommerce_login_form" ); ?>

						<p class="form-row">
							<label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
								<input type="checkbox" id="rememberme" value="forever" name="rememberme"
									   class="woocommerce-form__input woocommerce-form__input-checkbox"/>
								<span><?php esc_html_e( "Remember me", "woocommerce" ); ?></span>
							</label>
							<?php wp_nonce_field( "woocommerce-login", "woocommerce-login-nonce" ); ?>
							<button type="submit" name="login" value="<?php esc_attr_e( "Log in", "woocommerce" ); ?>"
									class="woocommerce-button button woocommerce-form-login__submit">
								<?php esc_html_e( "Log in", "woocommerce" ); ?>
							</button>
						</p>

						<?php do_action( "dom_login_form_end" ); ?>
						<?php do_action( "woocommerce_login_form_end" ); ?>

					</form>
				</div>

				<?php do_action( "dom_after_customer_login_form" ); ?>
				<?php do_action( "woocommerce_after_customer_login_form" ); ?>

			</article>

		</main>
	</div>

<?php get_footer( apply_filters( 'dom_login_footer_name', '' ) ) ?>