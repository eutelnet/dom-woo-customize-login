<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<?php get_header( apply_filters( 'dom_register_header_name', '' ) ) ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<article id="page-register" class="<?php echo apply_filters( 'dom_register_article_class', 'dom-register' ) ?>">

			<?php do_action( 'dom_before_customer_register_form' ); ?>
			<?php do_action( 'woocommerce_before_customer_register_form' ); ?>

			<form method="post" <?php do_action( 'woocommerce_register_form_tag' ); ?>
				  class="woocommerce-form woocommerce-form-register register">

				<?php do_action( 'dom_register_form_start' ); ?>
				<?php do_action( 'woocommerce_register_form_start' ); ?>

				<?php if ( get_option( 'woocommerce_registration_generate_username' ) === 'no' ) : ?>

					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="reg_username">
							<?php esc_html_e( 'Username', 'woocommerce' ); ?>&nbsp;<span class="required">*</span>
						</label>
						<input type="text" name="username" id="reg_username" autocomplete="username"
							   class="woocommerce-Input woocommerce-Input--text input-text"
							   value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
					</p>
				<?php endif; ?>

				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="reg_email">
						<?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span>
					</label>
					<input type="email" name="email" id="reg_email" autocomplete="email"
						   class="woocommerce-Input woocommerce-Input--text input-text"
						   value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
				</p>

				<?php if ( get_option( 'woocommerce_registration_generate_password' ) === 'no' ) : ?>
					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="reg_password">
							<?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span>
						</label>
						<input type="password" class="woocommerce-Input woocommerce-Input--text input-text"
							   name="password"
							   id="reg_password" autocomplete="new-password"/>
					</p>
				<?php else : ?>

					<p>
						<?php esc_html_e( 'A password will be sent to your email address.', 'woocommerce' ); ?>
					</p>

				<?php endif; ?>

				<?php do_action( 'dom_register_form' ); ?>
				<?php do_action( 'woocommerce_register_form' ); ?>

				<p class="woocommerce-FormRow form-row">
					<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
					<button type="submit" name="register"
							class="woocommerce-Button button"
							value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>">
						<?php esc_html_e( 'Register', 'woocommerce' ); ?>
					</button>
				</p>

				<?php do_action( 'dom_register_form_end' ); ?>
				<?php do_action( 'woocommerce_register_form_end' ); ?>

			</form>

			<?php do_action( "dom_after_customer_register_form" ); ?>
			<?php do_action( "woocommerce_after_customer_register_form" ); ?>

		</article>
	</main>
</div>

<?php get_footer( apply_filters( 'dom_register_footer_name', '' ) ) ?>
