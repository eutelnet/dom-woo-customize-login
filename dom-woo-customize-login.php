<?php

/**
 * @link                https://dev-o-matic.com/
 * @since               0.1.
 * @package             Dom/Woo_Customize_Login
 *
 * @wordpress-plugin
 * Plugin Name:         Woo Customized Login
 * Plugin URI:          https://dev-o-matic.com/plugins/dom-woo-customize-login
 * Description:         Adds easy templating and custom URLs for login/register/forgot password page
 * Version:             0.1.1
 * Author:              Dev-O-Matic
 * Author URI:          https://dev-o-matic.com/
 * License:             GPL-2.0+
 * License URI:         http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:         dom-woo-customize-login
 * Domain Path:         /languages
 */

use DOM\Woo_Customize_Login\Includes\Activator;
use DOM\Woo_Customize_Login\Includes\Deactivator;
use DOM\Woo_Customize_Login\Includes\Init;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 0.1.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'DOM_WOO_CUSTOMIZE_LOGIN_VERSION', '0.1.1' );
define( 'DOM_WOO_CUSTOMIZE_LOGIN_TEXT_DOMAIN', 'dom-woo-customize-login' );

require_once plugin_dir_path( __FILE__ ) . 'vendor/dom-autoload.php';

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-dom-woo-customize-login-activator.php
 */
function activate_dom_woo_customize_login() {
	Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-dom-woo-customize-login-deactivator.php
 */
function deactivate_dom_woo_customize_login() {
	Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_dom_woo_customize_login' );
register_deactivation_hook( __FILE__, 'deactivate_dom_woo_customize_login' );

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    0.1.0
 */
function run_dom_woo_customize_login() {

	$plugin = new Init();
	$plugin->run();

}

run_dom_woo_customize_login();
